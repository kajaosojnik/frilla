/*--HEADER ON SCROLL--*/

// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function (event) {
    didScroll = true;
});

setInterval(function () {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight) {
        // Scroll Down
        $('header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if (st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}


//ADD TO CART BUTTONS

const solo = document.querySelectorAll(".solo");
const bff = document.querySelectorAll(".bff");
const fam = document.querySelectorAll(".fam");

for(let i=0; i<solo.length; i++){
    solo[i].setAttribute("href",cartLink + id);
}

for(let i=0; i<bff.length; i++){
    bff[i].setAttribute("href",cartLink + id + "," + id);
}

for(let i=0; i<fam.length; i++){
    fam[i].setAttribute("href",cartLink + id + "," + id + "," + id);
}


// /*---QUANTITY---*/



const addToCartBtns = document.getElementsByClassName("addToCart");

for ( let i=0; i<addToCartBtns.length; i++){
    addToCartBtns[i].addEventListener("click", function(){
        const quantityValue = document.getElementById("quantity").value;
        if(quantityValue === "1"){
            addToCartBtns[i].setAttribute("href",cartLink + id);
        } else if(quantityValue === "2"){
            addToCartBtns[i].setAttribute("href",cartLink + id + "," + id );
        } else if(quantityValue === "3"){
            addToCartBtns[i].setAttribute("href",cartLink + id + "," + id + "," + id );
        } else if(quantityValue === "4"){
            addToCartBtns[i].setAttribute("href",cartLink + id + "," + id + "," + id + "," + id );
        } else if(quantityValue === "5"){
            addToCartBtns[i].setAttribute("href",cartLink + id + "," + id + "," + id + "," + id + "," + id );
        }
    })
}


/*--DELIVERY DATE CALCULATION--*/

function setShipDate() {
    // element to write date to
    var shipdate = document.getElementById("js-ship-date");
    // Set start date to today - end date in 2 days
    var startDate = new Date();
    var endDate = "", noOfDaysToAdd = 1, count = 0;
    // Map numeric month to name
    var month = new Array();
    month[0] = "styczeń";
  month[1] = "luty";
  month[2] = "marzec";
  month[3] = "kwiecień";
  month[4] = "maj";
  month[5] = "czerwiec";
  month[6] = "lipiec";
  month[7] = "sierpień";
  month[8] = "wrzesień";
  month[9] = "październik";
  month[10] = "listopad";
  month[11] = "grudzień";
  var weekday = new Array();
  weekday[0] = "niedziela";
  weekday[1] = "poniedziałek";
  weekday[2] = "wtorek";
  weekday[3] = "środa";
  weekday[4] = "czwartek";
  weekday[5] = "piątek";
  weekday[6] = "sobota";

    let today = new Date();
    let day = today.getDay();
    let plusDays = 2;
    // Preverjanje ure
    if (day <= 5 && day !== 0 && today.getHours() >= 9)
        plusDays += 1;
    if (day === 4 && today.getHours() >= 9)
        plusDays += 3;
    if (day >= 5) // Petek, sobota
        plusDays += 4;
    if (day === 0) // Nedelja
        plusDays += 2;
    endDate = new Date(today.getDate() + plusDays);
    endDate = new Date(startDate.setDate(startDate.getDate() + plusDays));

    // update shipdate HTML
    shipdate.innerHTML = weekday[endDate.getDay()] + ", " + endDate.getDate() + '. ' + month[endDate.getMonth()] + ' ' + endDate.getFullYear() + ' ';
}

setShipDate();


/*---REVIEW LOGICS---*/

//review toggle

var reviewToggle = document.getElementById("review-toggle");
var reviewBody = document.getElementById("review-body");

reviewToggle.addEventListener("click", function(){
    reviewBody.classList.toggle("toggle-review");
})

//form validation

var reviewName = document.getElementById("name");
var reviewEmail = document.getElementById("email");
var reviewText = document.getElementById("textarea");


/*---THANK YOU FOR SUBMITTING REVIEW MESSAGE---*/

var sumbitBtn = document.getElementById("submit-review");

sumbitBtn.addEventListener("click", function(event){
    event.preventDefault();
    const thankYouSpan = this.nextElementSibling;
    if(reviewName.value === "" || reviewEmail.value === "" || reviewText.value === ""){
      thankYouSpan.innerHTML = "Please fill in the required fields."
    } else {
        thankYouSpan.innerHTML = "Thank you for submitting your review."
        setTimeout(function(){
            thankYouSpan.innerHTML = "";
            reviewName.value = "";
            reviewEmail.value = "";
            reviewText.value = "";
            }, 2000);
    }

});


// /*---PLAY/PAUSE VIDEO ON SCROLL---*/


window._wq = window._wq || [];

// target our video by the first 3 characters of the hashed ID
_wq.push({
    id: "676zyzztsh", onReady: function (video) {

        // mute video
        video.mute();

        // helper function for viewPort calculation
        var isInViewport = function (elem) {
            var bounding = elem.getBoundingClientRect();
            return (
                bounding.top >= 0 &&
                bounding.left >= 0 &&
                bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        };

        // find video element
        var videoElement = document.getElementById('video');

        // listen to scroll changes
        window.addEventListener('scroll', function () {
            if (isInViewport(videoElement)) {
                video.play();
            } else {
                video.pause();
            }
        }, false);
    }
});

     /*---POP UPS 2--*/

    const females = ["Anna", "Maria", "Joanna", "Natalia", "Eliza", "Daria", "Agnieszka", "Sylwia", "Krystian", "Urszula", "Paulina", "Manuela", "Justyna", "Alexandra", "Stanisława", "Bożena", "Natalia", "Marianna", "Czesława", "Iwona", "Jolanta", "Grażyna", "Karolina", "Helena", "Dorota", "Marta", "Beata", "Irena", "Halina", "Aleksandra", "Joanna", "Magdalena", "Teresa", "Iwona", "Zofia", "Elżbieta", "Julia", "Ewa", "Krystyna", "Klaudia", "Ksenia", "Laura", "Lili", "Katarzyna", "Malwina", "Janina", "Barbara", "Monika", "Danuta", "Jadwiga"];
    const males = ["Artur", "Wiesław", "Dawid", "Sławomir", "Jarosław", "Mirosław", "Marian", "Janusz", "Kamil", "Maciej", "Kazimierz", "Rafał", "Robert", "Henryk", "Jakub", "Ryszard", "Wojciech", "Mariusz", "Dariusz", "Mateusz", "Tadeusz", "Jerzy", "Paweł", "Marcin", "Michał", "Marek", "Grzegorz", "Józef", "Łukasz", "Adam", "Tomasz", "Stanisław", "Jan", "Andrzej", "Krzysztof", "Cezary", "Norbert", "Wacław", "Rafał", "Bronisław", "Filip", "Sylwester", "Włodzimierz", "Aleksander", "Krystian", "Franciszek", "Stefan", "Radosław", "Dominik", "Zygmunt"];

    const people = females.concat(males);

    const locations = ["Pułtuska", "Zielonej Góry", "Bydgoszczy", "Krakowa", "Kraków", "Gniezna", "Gdyni", "Poznania", "Tarnobrzegu", "Brzeska", "Kołobrzegu", "Warszawy", "Czechowic-Dziecic", "Wrocławia", "Warszawa", "Szczecin", "Łódź", "Wrocław", "	Zielona Góra", "Gdańsk", "Poznań", "Świnoujście", "Dąbrowa Górnicza", "Zabrze", "Żabno", "Sulechów", "Sulęcin", "Szamotuły", "Radzymin", "Reda", "Pogorzela", "Połaniec", "Poznań", "Prusice", "Przedecz", "Ogrodzieniec", "Olesno", "Narol", "Nowe", "Miłosław", "Miastko", "Łask", "Luboń", "Kwidzyn", "Krośniewice", "Kock", "Jordanów", "Inowrocław", "Gryfino", "Głowno", "Elbląg", "Frampol", "Dębica"]
    const quantity = ["1x", "1x", "1x", "2x", "1x", "1x", "1x", "3x"]
    const textBeforeFemale = " kupiła ";
    const textBeforeMale = " kupił ";
    const textAfter = " Komplet: niewidoczny biustonosz push-up vigo 1+1</strong>";
    const popUp = document.getElementById("pop-up");
    const popUpSpan = document.getElementById("pop-up-span")

    function random(x) {
        randoms = x[Math.floor(Math.random() * x.length)];
        return randoms;
    }

    function open() {
        popUp.style.opacity = "1";
        popUp.style.left = '0';
        popUp.style.transition = "all ease .5s";
    }

    function close() {
        popUp.style.left = '200rem';
        popUp.style.transition = "all ease 1s";
    }

    function reset() {
        popUp.style.opacity = "0";
        popUp.style.transition = "all 0s";
        popUp.style.left = "-24rem";
    }


    function PopUpFunction() {
        let randomPerson = random(people);
        random(locations);
        random(quantity);

        if (females.includes(randomPerson)) {
            popUpSpan.innerHTML = "<span>" + randomPerson + " z " + random(locations) + textBeforeFemale + "<strong>" + random(quantity) + textAfter + "</span>";
            // console.log("female");
        } else {
            popUpSpan.innerHTML = "<span>" + randomPerson + " z " + random(locations) + textBeforeMale + "<strong>" + random(quantity) + textAfter + "</span>";
            // console.log("male");
        }

        setTimeout("open()", 1000);
        setTimeout("close()", 4000);
        setTimeout("reset()", 5000);
    }

    setInterval(PopUpFunction, 34000);

    /*---HEART--*/

    document.getElementById("like").addEventListener("click", function(){
        this.classList.toggle("red-heart");

    });


    // before after slider

// I hope this over-commenting helps. Let's do this!
// Let's use the 'active' variable to let us know when we're using it
let active = false;

// First we'll have to set up our event listeners
// We want to watch for clicks on our scroller
document.querySelector('.scroller').addEventListener('mousedown',function(){
    active = true;
    // Add our scrolling class so the scroller has full opacity while active
    document.querySelector('.scroller').classList.add('scrolling');
});
// We also want to watch the body for changes to the state,
// like moving around and releasing the click
// so let's set up our event listeners
document.body.addEventListener('mouseup',function(){
    active = false;
    document.querySelector('.scroller').classList.remove('scrolling');
});
document.body.addEventListener('mouseleave',function(){
    active = false;
    document.querySelector('.scroller').classList.remove('scrolling');
});

// Let's figure out where their mouse is at
document.body.addEventListener('mousemove',function(e){
    if (!active) return;
    // Their mouse is here...
    let x = e.pageX;
    // but we want it relative to our wrapper
    x -= document.querySelector('.wrapper').getBoundingClientRect().left;
    // Okay let's change our state
    scrollIt(x);
});

// Let's use this function
function scrollIt(x){
    let transform = Math.max(0,(Math.min(x,document.querySelector('.wrapper').offsetWidth)));
    document.querySelector('.after').style.width = transform+"px";
    document.querySelector('.scroller').style.left = transform-25+"px";
}

// Let's set our opening state based off the width,
// we want to show a bit of both images so the user can see what's going on
scrollIt(150);

// And finally let's repeat the process for touch events
// first our middle scroller...
document.querySelector('.scroller').addEventListener('touchstart',function(){
    active = true;
    document.querySelector('.scroller').classList.add('scrolling');
});
document.body.addEventListener('touchend',function(){
    active = false;
    document.querySelector('.scroller').classList.remove('scrolling');
});
document.body.addEventListener('touchcancel',function(){
    active = false;
    document.querySelector('.scroller').classList.remove('scrolling');
});
