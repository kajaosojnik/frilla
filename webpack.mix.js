const mix = require('laravel-mix')

require('laravel-mix-tailwind')
require('laravel-mix-purgecss')

mix.scripts([
        './assets/js/app.js'
    ],
    './assets/js/dist/app.js')
    .scripts('./node_modules/slick-carousel/slick/slick.min.js', './assets/js/dist/slick.min.js')
    .sass('./assets/scss/app.scss', './assets/css')
    .styles([
            './node_modules/slick-carousel/slick/slick.css',
            './node_modules/slick-carousel/slick/slick-theme.css',
            './assets/css/slick.css'

        ], './assets/css/vendor.css'
    )
    .options({
        autoprefixer: {
            options: {
                browsers: [
                    'last 6 versions',
                ]
            }
        }
    })
    .tailwind()
    .purgeCss({
        globs: [
            path.join(__dirname, '/**/*.html'),
        ],
        folders: [
            './assets/scss'
        ]
    });